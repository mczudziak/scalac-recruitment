package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._


class OrgListControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {
  "OrgListController GET" should {

    "get a GitHub rank for ScalaConsultants" in {
      val scalacRet: JsValue = Json.parse("""[{"name":"jczuchnowski","contributions":773},{"name":"marioosh","contributions":149},{"name":"tues","contributions":39},{"name":"ernestochero","contributions":22},{"name":"mayonesa","contributions":11},{"name":"marekklis","contributions":7},{"name":"lkuczera","contributions":6},{"name":"molowny","contributions":0},{"name":"lkff","contributions":0},{"name":"danieletorelli","contributions":0}]""")

      val controller = new OrgListController(stubControllerComponents())
      val rankJson = controller.getByOrgName("ScalaConsultants").apply(FakeRequest(GET, "/org/ScalaConsultants/contributors"))

      status(rankJson) mustBe OK
      contentType(rankJson) mustBe Some("application/json")
      contentAsJson(rankJson) mustBe scalacRet
    }

    "get a GitHub rank for twitter" in {
      val scalacRet: JsValue = Json.parse("""[{"name":"cacoco","contributions":1230},{"name":"thinkingfish","contributions":803},{"name":"roanta","contributions":434},{"name":"kwlzn","contributions":170},{"name":"willnorris","contributions":152},{"name":"sagemintblue","contributions":118},{"name":"niw","contributions":87},{"name":"ryancouto","contributions":81},{"name":"slyphon","contributions":70},{"name":"grimreaper","contributions":42},{"name":"andypiper","contributions":38},{"name":"wickman","contributions":36},{"name":"juliaferraioli","contributions":30},{"name":"taylorleese","contributions":25},{"name":"OrkoHunter","contributions":19},{"name":"kgoodier","contributions":14},{"name":"pavanky","contributions":7},{"name":"couch","contributions":6},{"name":"casassg","contributions":4},{"name":"MichaelEvans","contributions":3},{"name":"sundresh","contributions":2},{"name":"stukra","contributions":2},{"name":"rondevera","contributions":1},{"name":"fmborghino","contributions":1},{"name":"endform","contributions":1},{"name":"tufandemir","contributions":0},{"name":"tdmackey","contributions":0},{"name":"skilldrick","contributions":0},{"name":"seanprashad","contributions":0},{"name":"richardvenneman","contributions":0},{"name":"objmagic","contributions":0},{"name":"nvgrw","contributions":0},{"name":"nkouevda","contributions":0},{"name":"mvisonneau","contributions":0},{"name":"jamesryanalexander","contributions":0},{"name":"emad-elsaid","contributions":0},{"name":"EliasMera","contributions":0},{"name":"codesue","contributions":0},{"name":"alykhantejani","contributions":0},{"name":"ahmetb","contributions":0}]""")

      val controller = new OrgListController(stubControllerComponents())
      val rankJson = controller.getByOrgName("twitter").apply(FakeRequest(GET, "/org/twitter/contributors"))

      status(rankJson) mustBe OK
      contentType(rankJson) mustBe Some("application/json")
      contentAsJson(rankJson) mustBe scalacRet
    }

    "get a GitHub rank for guardian" in {
      val scalacRet: JsValue = Json.parse("""[{"name":"sndrs","contributions":4314},{"name":"kelvin-chappell","contributions":3452},{"name":"NathanielBennett","contributions":1402},{"name":"JustinPinner","contributions":1248},{"name":"mxdvl","contributions":1149},{"name":"johnduffell","contributions":1111},{"name":"joelochlann","contributions":676},{"name":"tomrf1","contributions":631},{"name":"adamnfish","contributions":502},{"name":"mchv","contributions":409},{"name":"rtyley","contributions":394},{"name":"arelra","contributions":363},{"name":"markjamesbutler","contributions":348},{"name":"davidfurey","contributions":345},{"name":"akash1810","contributions":272},{"name":"coldlink","contributions":182},{"name":"tjmw","contributions":105},{"name":"vlbee","contributions":96},{"name":"oliverlloyd","contributions":95},{"name":"rupertbates","contributions":80},{"name":"paulbrown1982","contributions":23},{"name":"paulmr","contributions":19},{"name":"kenoir","contributions":6},{"name":"ob6160","contributions":0},{"name":"michaelclapham","contributions":0},{"name":"MarSavar","contributions":0},{"name":"ashishpuliyel","contributions":0},{"name":"andrew-nowak","contributions":0},{"name":"alinaboghiu","contributions":0}]""")

      val controller = new OrgListController(stubControllerComponents())
      val rankJson = controller.getByOrgName("guardian").apply(FakeRequest(GET, "/org/guardian/contributors"))

      status(rankJson) mustBe OK
      contentType(rankJson) mustBe Some("application/json")
      contentAsJson(rankJson) mustBe scalacRet
    }

    "get a GitHub rank for cloudflare" in {
      val scalacRet: JsValue = Json.parse("""[{"name":"grittygrease","contributions":243},{"name":"cbroglie","contributions":172},{"name":"terinjokes","contributions":148},{"name":"dknecht","contributions":89},{"name":"patryk","contributions":72},{"name":"dqminh","contributions":42},{"name":"dotjs","contributions":32},{"name":"manatarms","contributions":27},{"name":"maxnystrom","contributions":18},{"name":"ejcx","contributions":15},{"name":"ghedo","contributions":14},{"name":"lgarofalo","contributions":13},{"name":"mihirjham","contributions":8},{"name":"junhochoi","contributions":8},{"name":"echtish","contributions":6},{"name":"kornelski","contributions":3},{"name":"hturan","contributions":3},{"name":"claucece","contributions":3},{"name":"kentonv","contributions":2},{"name":"nvartolomei","contributions":1},{"name":"inikulin","contributions":1},{"name":"aaranmcguire","contributions":1},{"name":"younggeeks","contributions":0},{"name":"xtuc","contributions":0},{"name":"WalshyDev","contributions":0},{"name":"vasturiano","contributions":0},{"name":"uhthomas","contributions":0},{"name":"tvarney","contributions":0},{"name":"tlianza","contributions":0},{"name":"taktv6","contributions":0},{"name":"skepticfx","contributions":0},{"name":"sid405","contributions":0},{"name":"scotchmist","contributions":0},{"name":"rita3ko","contributions":0},{"name":"rhoml","contributions":0},{"name":"renandincer","contributions":0},{"name":"porty","contributions":0},{"name":"ObsidianMinor","contributions":0},{"name":"nkcmr","contributions":0},{"name":"nilslice","contributions":0},{"name":"mstremante","contributions":0},{"name":"migueldemoura","contributions":0},{"name":"mgirouard","contributions":0},{"name":"markpash","contributions":0},{"name":"Margatroid","contributions":0},{"name":"lukevalenta","contributions":0},{"name":"larcher","contributions":0},{"name":"kkrum","contributions":0},{"name":"jroyal","contributions":0},{"name":"jrf0110","contributions":0},{"name":"jensechu","contributions":0},{"name":"jejenone","contributions":0},{"name":"janikrabe","contributions":0},{"name":"ignatk","contributions":0},{"name":"harrishancock","contributions":0},{"name":"GregBrimble","contributions":0},{"name":"gabi-250","contributions":0},{"name":"Electroid","contributions":0},{"name":"eaufavor","contributions":0},{"name":"codewithkristian","contributions":0},{"name":"chhetripradeep","contributions":0},{"name":"cfcasey","contributions":0},{"name":"bwesterb","contributions":0},{"name":"bretthoerner","contributions":0},{"name":"armfazh","contributions":0},{"name":"AlecGoncharow","contributions":0},{"name":"Albertozhao","contributions":0},{"name":"ahrex","contributions":0},{"name":"a-robinson","contributions":0},{"name":"46bit","contributions":0}]""")

      val controller = new OrgListController(stubControllerComponents())
      val rankJson = controller.getByOrgName("cloudflare").apply(FakeRequest(GET, "/org/cloudflare/contributors"))

      status(rankJson) mustBe OK
      contentType(rankJson) mustBe Some("application/json")
      contentAsJson(rankJson) mustBe scalacRet
    }

    "get a GitHub rank for nonexistent organization" in {
      val scalacRet: JsValue = Json.parse("""[]""")

      val controller = new OrgListController(stubControllerComponents())
      val rankJson = controller.getByOrgName("blebleble").apply(FakeRequest(GET, "/org/blebleble/contributors"))

      status(rankJson) mustBe OK
      contentType(rankJson) mustBe Some("application/json")
      contentAsJson(rankJson) mustBe scalacRet
    }
  }
}
