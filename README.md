# SCALAC GITHUBRANK CHALLANGE SOLUTION

Author: Maksymilian Czudziak

This is my solution to recrutitment task assigned to me by ScalaC.

## Tasks

- [x] use GitHub REST API v3 (<https://developer.github.com/v3/>)
- [x] return a list sorted by the number of contributions made by the developer
to all repositories for the given organization.
- [x] respond to a GET request at port 8080 and address `/org/{org_name}/contributors`
- [x] respond with JSON, which should be a list of contributors in the following
format: { “name”: <contributor_login>, “contributions”: <no_of_contributions> }
- [x] handle GitHub’s API rate limit restriction using a token that can be set?
as an environment variable of name GH_TOKEN
- [x] friendly hint: GitHub API returns paginated responses. You should take it
into account if you want the result to be accurate for larger organizations.

TODO - list:
(Stuff that I haven't found time for, but know how to do.)

- [ ] Parallelization of the requests loop
- [ ] Test parallelization
- [ ] HTTP Requests Cashing
- [ ] HTTP Requests Paging

## Setup

This project uses `sbt`, it can be used for an interactive shell, but also

- `sbt "run 8080"` starts server on the port `8080`
then it can be tested with `curl -v localhost:8080/org/ScalaConsultants/contributors`
- `sbt test` starts tests
