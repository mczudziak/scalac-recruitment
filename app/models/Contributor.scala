package models

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import github4s.Github
import org.http4s.client.{Client, JavaNetClientBuilder}

import javax.inject._
import github4s.domain.{User, Pagination, Repository}
import java.time.LocalDateTime

case class Organization(name: String, contributors : List[Contributor], fetchTimestamp: LocalDateTime)
case class Contributor(id:Long, name: String, contributions: Option[Int])


@Singleton
class GitHubFetcher () {
    val httpClient: Client[IO] = {
        JavaNetClientBuilder[IO].create
    }
    
    val accessToken = sys.env.get("GH_TOKEN")
    val gh = Github[IO](httpClient, accessToken)

    def getOrganizationMembersAndRepositories(orgName: String) : (List[Contributor], List[Repository]) = {
        val listMembersIO = gh.organizations.listMembers(orgName, None, None, Some(Pagination(1, 100)))
        val listOrgReposIO = gh.repos.listOrgRepos(orgName, None, Some(Pagination(1, 100)))

        try{
            val listMembersLR = listMembersIO.unsafeRunSync()
            val listOrgReposLR = listOrgReposIO.unsafeRunSync()
            (listMembersLR.result, listOrgReposLR.result) match {
                case (Left(e1), Left(e2))  => throw new Exception(s"List members and repositories went wrong: ${e1.getMessage} ${e2.getMessage}")
                case (_, Left(e))  => throw new Exception(s"List repositories went wrong: ${e.getMessage}")
                case (Left(e), _)  => throw new Exception(s"List members went wrong: ${e.getMessage}")
                case (Right(r1), Right(r2)) => (r1.map(m => Contributor(m.id, m.login, m.contributions)), r2)
            }
        }
        catch {
            case e:Exception =>
            println(s"Listing members or organization repositories went wrong: ${e.getMessage}")
            (List(),List())
        }
    }

    def getRepositoryContributors (owner: String, name: String) : List[User] = {
        val listContributorsIO = gh.repos.listContributors(owner, name, Some("false"), Some(Pagination(1,100)))
        try{
            val listContributorsLR = listContributorsIO.unsafeRunSync()
            listContributorsLR.result match {
                case Left(e)  => throw new Exception(s"Listing contributors is empty: ${e.getMessage}")
                case Right(r) => r
            }
        }
        catch {
            case e:Exception =>
            println(s"Listing contributors went wrong: ${e.getMessage}")
            List()
        }
    }
    
    def getGitHubRank (orgName: String) : List[Contributor] = {
        val (members, repos) = getOrganizationMembersAndRepositories(orgName)
        val membersIDs = members.map(m => m.id)
    
        val contributionsFromOrganization = 
            (for (r <- repos) yield {
                val contributors = getRepositoryContributors(orgName, r.name)
                val contributorsIDs = contributors.map(c => c.id)
                
                contributors.filter(x => (contributorsIDs.intersect(membersIDs)).contains(x.id))
            }).flatten
        
        val accumulatedUsers = for (m <- members) yield {
            val calculateContrubutions = {Some(
                contributionsFromOrganization.filter(m.id == _.id).foldLeft(0)
                ((acc:Int, u:User) => {
                    u.contributions match {
                        case Some (x) => acc + x
                        case None => acc
                        }
                })
            )}
            m.copy(contributions = calculateContrubutions)
        }
        accumulatedUsers.sortBy(_.contributions).reverse.take(100)
    }
}