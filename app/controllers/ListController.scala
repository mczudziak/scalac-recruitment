package controllers


import javax.inject._
import models._
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import scala.collection.mutable

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit.MINUTES


@Singleton 
class OrgListController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {
    private val orgList = new mutable.ListBuffer[Organization]()

    implicit val ContributorWrites = new Writes[Contributor] {
    def writes(contributor: Contributor) = Json.obj(
        "name"  -> contributor.name,
        "contributions" -> contributor.contributions
        )
    }
    implicit val OrganizationWrites = new Writes[Organization] {
    def writes(organization: Organization) = Json.obj(
        "name"      -> organization.name,
        "contributors"  -> organization.contributors,
        "timestamp" -> organization.fetchTimestamp
        )
    }
        
    val fetcher = new GitHubFetcher()

    def getByOrgName(orgName: String): Action[AnyContent] = Action {
        val contributors : List[Contributor] = orgList.find((org) => 
            (org.name == orgName && MINUTES.between(org.fetchTimestamp, LocalDateTime.now()) < 10)) match {
            case Some (cachedOrganization) => cachedOrganization.contributors
            case None => {
                val contributors = fetcher.getGitHubRank(orgName)
                orgList += new Organization(orgName, contributors, LocalDateTime.now())
                contributors
            }
        }
        Ok(Json.toJson(contributors))
    }
}
